
# Catalogovinos

Este programa está creado con el propósito de funcionar como un catálogo, en el que el usuario pueda incluir y poder ver impreso por pantalla los vinos que este haya añadido, teniendo estos diversos atributos.

## Instalación

El código puede ser descargado mediante Bitbucket.

## Uso del *make*

# Suprime los directorios bin y html, elimina los ficheros .jar, los .class y .txt.
    make limpiar

## Crea el directorio bin y allí almacena los .class que ha compilado durante la ejecución del make. 
    make compilar

# Crea el directorio html y los ficheros .html.
    make html
    
# Crea el .jar
    make jar

# Limpia, compila, crea los .jar, los html y ejecuta la interfaz.
    make catalogo
    
# Diagrama de clases

![](https://bitbucket.org/Julen_86/winecatalog/raw/b447f8159fc6d712597e3a4be4f7943ef9fb766e/catalogowines/class%20diagram.png)

## License
Copyright [2021] [Julen Lozano Leal] 
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. 

