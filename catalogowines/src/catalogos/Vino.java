/* Copyright [2021] [Julen Lozano Leal]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.

You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. */


package src.catalogos;

public class Vino{

	private String marca;
	private String tipo;
	private int precio; 

	// Constructor sin parámetros
	public Vino(){}

	// Constructor con un solo parámetro para usar en el borrado del Array List
	public Vino(String marca){
	this.marca = marca;
	}

	 public Vino(String marca, String tipo){
                this.marca = marca;
                this.tipo = tipo;
	}

	// Constructor con parámetros de los objetos de la clase vino
	public Vino(String marca, String tipo, int precio) {
		this.marca = marca;
		this.tipo = tipo;
		this.precio = precio;
	}

	// Método que le dice al programa que todos aquellos objetos con el mismo atributo son el mismo objeto a la hora de compararlos
	public boolean equals(Object vino){
		Vino v = (Vino) vino;
		return this.marca.equals(v.marca);
	}

	// Crea una cadena de chars definida por el programador, para así que pueda salir impreso por pantalla esa cadena con los atributos y no la dirección  de memoria
	public String toString(){
		return  marca + ", " + tipo + ", " + precio;
	}

	public String getMarca(){
		return marca;
	}

	public String getTipo(){
		return tipo;
	}

	public int getPrecio(){
		return precio;
	}
}
