/* Copyright [2021] [Julen Lozano Leal]
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.

You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. */

package src.catalogos;
import java.io.*;
import java.util.*;

public class Catalogo{

	private ArrayList<Vino> catalogo = new ArrayList<>();	
	private String nombreFichero = "vinos.txt";
	
	
	public Catalogo() {
		cargarVinos();
	}

	public void add(Vino v){
		catalogo.add(v);
	}
	
	public void annadir(Vino v){
		catalogo.add(v);
		volcarVinos();
 		System.out.println("Vino incluido o modificado de manera satisfactoria.");
	}
	
	// Permite borrar objetos del ArrayList 
	public void borrar(Vino v){
		catalogo.remove(v);
		volcarVinos();
		System.out.println("Vino borrado de manera satisfactoria.");
	}
	
	// Lee el fichero en el que se guardaban los objetos y este convierte las líneas de chars en objetos que guaradar en el array
	private void cargarVinos(){
			try{
				File fichero = new File(nombreFichero);
				fichero.createNewFile(); 
				Scanner sc = new Scanner(fichero);
				sc.useDelimiter(",|\n");
				while(sc.hasNext()){
					catalogo.add(new Vino (sc.next(), sc.next(), sc.nextInt()));
				}
		}catch(IOException ex){
			System.out.println("El catalogo se encuentra vacio actualmente");
		}
	}
	
	// Nos permite escribir en un fichero .txt los objetos del array para así ppoder guardarse pese al cierre del programa
	private void volcarVinos(){
		try{
			FileWriter fw = new FileWriter(nombreFichero);
			for(Vino v : catalogo){
				fw.write(v.getMarca() + "\n");
				fw.write(v.getTipo() + "\n");
				fw.write(v.getPrecio() + "\n");

			}
			
			fw.close();
		}catch(IOException ex){
			System.err.print(ex);
		}
	}
	
	// Nos genera una hoja de calculo que guarda los vinos y sus atributos
	public void generarHojaCalculo(){
		try{
			FileWriter fw = new FileWriter(nombreFichero);
			fw.write("Marca,Tipo,Precio\n");
			fw.write(toString().trim());
			fw.close();
                  }catch(IOException ex){
			  System.err.println(ex);
		  }
	}


	public String toString(){
		StringBuilder sb = new StringBuilder();
		for(Vino v  : catalogo)
		sb.append(v + "\n");

	return sb.toString();
	}	
}

